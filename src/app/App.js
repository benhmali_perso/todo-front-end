import React, {Component} from 'react';
import './App.css';
import {Route, Switch, withRouter} from 'react-router-dom';

import {getCurrentUser} from '../utils/ApiUtils';
import {ACCESS_TOKEN} from '../constants';
import Login from '../user/login/index';
import Signup from '../user/signup/index';
import LoadingIndicator from '../common/LoadingIndicator';
import AppHeader from '../common/AppHeader';
import PrivateRoute from '../common/PrivateRoute';
import NewTodo from '../todo/NewTodo';

import {Layout, notification} from 'antd';
import TodoList from '../todo/TodoList';

const {Content} = Layout;

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentUser: null,
			isAuthenticated: false,
			isLoading: false
		};
		this.handleLogout = this.handleLogout.bind(this);
		this.loadCurrentUser = this.loadCurrentUser.bind(this);
		this.handleLogin = this.handleLogin.bind(this);

		notification.config({
			placement: 'topRight',
			top: 70,
			duration: 3,
		});
	}

	loadCurrentUser() {
		this.setState({
			isLoading: true
		});
		getCurrentUser()
			.then(response => {
				this.setState({
					currentUser: response.username,
					isAuthenticated: true,
					isLoading: false
				});
			}).catch(error => {
				this.setState({
					isLoading: false
				});
			});
	}

	componentDidMount() {
		this.loadCurrentUser();
	}

	handleLogout(redirectTo = '/', notificationType = 'success', description = 'You\'re successfully logged out.') {
		localStorage.removeItem(ACCESS_TOKEN);

		this.setState({
			currentUser: null,
			isAuthenticated: false
		});

		this.props.history.push(redirectTo);

		notification[notificationType]({
			message: 'Todo App',
			description: description,
		});
	}

	handleLogin() {
		notification.success({
			message: 'Todo App',
			description: 'You\'re successfully logged in.',
		});
		this.loadCurrentUser();
		this.props.history.push('/');
	}

	render() {
		if (this.state.isLoading) {
			return <LoadingIndicator/>;
		}
		return (
			<Layout className="app-container">
				<AppHeader isAuthenticated={this.state.isAuthenticated}
					currentUser={this.state.currentUser}
					onLogout={this.handleLogout}/>

				<Content className="app-content">
					<div className="container">
						<Switch>
							<Route exact path="/"
								render={(props) => <TodoList isAuthenticated={this.state.isAuthenticated}
									currentUser={this.state.currentUser}
									handleLogout={this.handleLogout} {...props} />}>
							</Route>
							<Route path="/login"
								render={(props) => <Login onLogin={this.handleLogin} {...props} />}/>
							<Route path="/signup" component={Signup}/>
							<PrivateRoute authenticated={this.state.isAuthenticated} path="/todo/new"
								component={NewTodo} handleLogout={this.handleLogout}/>
						</Switch>
					</div>
				</Content>
			</Layout>
		);
	}
}

export default withRouter(App);
