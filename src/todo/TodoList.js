import React, {Component} from 'react';
import {Button, Icon} from 'antd';
import {withRouter} from 'react-router-dom';
import {getUserTodos} from '../utils/ApiUtils';
import Todo from './Todo';
import LoadingIndicator from '../common/LoadingIndicator';
import {TODO_LIST_SIZE} from '../constants';
import './Todo.css';

class TodoList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			todos: [],
			page: 0,
			size: 10,
			totalElements: 0,
			totalPages: 0,
			last: true,
			isLoading: false,
		};
		this.loadTodoList = this.loadTodoList.bind(this);
	}

	loadTodoList(page = 0, size = TODO_LIST_SIZE) {
		let promise;
		promise = getUserTodos(this.props.currentUser, page, size);

		if (!promise) {
			return;
		}

		this.setState({
			isLoading: true,
		});

		promise
			.then((response) => {
				const todos = this.state.todos.slice();

				this.setState({
					todos: todos.concat(response.content),
					page: response.page,
					size: response.size,
					totalElements: response.totalElements,
					totalPages: response.totalPages,
					last: response.last,
					isLoading: false,
				});
			}).catch((error) => {
				this.setState({
					isLoading: false,
				});
			});
	}

	componentDidMount() {
		this.loadTodoList();
	}


	componentDidUpdate(nextProps) {
		if (this.props.isAuthenticated !== nextProps.isAuthenticated) {
			// Reset State
			this.setState({
				todos: [],
				page: 0,
				size: 10,
				totalElements: 0,
				totalPages: 0,
				last: true,
				isLoading: false,
			});
			this.loadTodoList();
		}
	}

	render() {
		const todoViews = [];
		this.state.todos.forEach((todo, todoIndex) => {
			todoViews.push(<Todo
				key={todo.id}
				todo={todo}
			/>);
		});

		return (
			<div className="todos-container">
				{todoViews}
				{
					!this.state.isLoading && this.state.todos.length === 0 ? (
						<div className="no-todos-found">
							<span>No Todos Found.</span>
						</div>
					) : null
				}
				{
					!this.state.isLoading && !this.state.last ? (
						<div className="load-more-todos">
							<Button type="dashed" disabled={this.state.isLoading}>
								<Icon type="plus"/>
								{' '}
                                Load more
							</Button>
						</div>) : null
				}
				{
					this.state.isLoading
						? <LoadingIndicator/> : null
				}
			</div>
		);
	}
}

export default withRouter(TodoList);
