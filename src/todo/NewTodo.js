import React, {Component} from 'react';
import {createTodo} from '../utils/ApiUtils';
import './NewTodo.css';
import {Button, Form, Input, notification, Select} from 'antd';

const Option = Select.Option;
const FormItem = Form.Item;
const {TextArea} = Input;

class NewTodo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			title: {
				text: ''
			},
			description: {
				text: ''
			},
			status: {
				text: ''
			}
		};
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleStatusChange = this.handleStatusChange.bind(this);
	}

	handleInputChange(event) {
		const target = event.target;
		const inputName = target.name;
		const inputValue = target.value;

		this.setState({
			[inputName]: {
				text: inputValue,
			}
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		const todoData = {
			titre: this.state.title.text,
			description: this.state.description.text,
			etat: this.state.status.text
		};

		createTodo(todoData)
			.then(response => {
				this.props.history.push('/');
			}).catch(error => {
				if (error.status === 401) {
					this.props.handleLogout('/login', 'error', 'You have been logged out. Please login create todo.');
				} else {
					notification.error({
						message: 'Todo App',
						description: error.message || 'Sorry! Something went wrong. Please try again!'
					});
				}
			});
	}

	handleStatusChange(event) {
		this.setState({
			status: {
				text: event
			}
		});
	}

	render() {
		return (
			<div className="new-todo-container">
				<h1 className="page-title">Create Todo</h1>
				<div className="new-todo-content">
					<Form onSubmit={this.handleSubmit} className="create-todo-form">
						<FormItem label="Task title" className="todo-form-row" hasFeedback>
							<Input
								size="large"
								name="title"
								autoComplete="off"
								placeholder="Task title"
								value={this.state.title.text}
								onChange={(event) => this.handleInputChange(event)}/>
						</FormItem>
						<FormItem label="Description" className="todo-form-row">
							<TextArea
								placeholder="Enter your description"
								style={{fontSize: '16px'}}
								autosize={{minRows: 3, maxRows: 6}}
								name="description"
								value={this.state.description.text}
								onChange={(event) => this.handleInputChange(event)}/>
						</FormItem>
						<FormItem className="todo-form-row">
							<Select
								name="status"
								onChange={(event) => this.handleStatusChange(event)}
							>
								<Option value="TODO">TODO</Option>
								<Option value="DOING">DOING</Option>
								<Option value="DONE">DONE</Option>
							</Select>
						</FormItem>
						<FormItem className="todo-form-row">
							<Button type="primary"
								htmlType="submit"
								size="large"
								className="create-todo-form-button">Create Todo</Button>
						</FormItem>
					</Form>
				</div>
			</div>
		);
	}
}

export default NewTodo;