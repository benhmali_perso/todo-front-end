import React, { Component } from 'react';
import './Todo.css';
import { Avatar } from 'antd';
import { Link } from 'react-router-dom';

class Todo extends Component {

	render() {
		return (
			<div className="todo-content">
				<div>
					<div className="todo-creator-info">
						<Link className="creator-link" to={'/'}>
							<Avatar className="todo-status">
								{this.props.todo.etat.description}
							</Avatar>
							<span className="todo-creator-name">
								{this.props.todo.createdBy.username}
							</span>
							<span className="todo-title">
								{this.props.todo.titre}
							</span>
							<span className="todo-creation-date">
								{this.props.todo.createdBy.createdAt}
							</span>
						</Link>
					</div>
					<div className="todo-description">
						{this.props.todo.description}
					</div>
				</div>
			</div>
		);
	}
}
export default Todo;
